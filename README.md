### Requirements
docker
docker-compose

### Install
Clone repository

Inside **docker** folder run:
```
docker-compose up
```
This might take a while to run since it will:
- install composer dependencies
- create **subway** database
- run migrations
- seed sandwich ingredients

### Usage
Application can be accessed through the address **http://127.0.0.1:8080**

Database can be access through address **http://127.0.0.1:3306** with user **root** and password: **secret**

There are no users created by default, they can be created/listed in **http://127.0.0.1:8080/user**

Also there's no open meal by default, it can be opened in **http://127.0.0.1:8080/meal**

### Application current state
Right now application allows:
- create users (http://192.168.15.5/user)
- list users (http://192.168.15.5/user)
- open registration for a new meal (http://127.0.0.1:8080/meal)
- user add a sandwich to the open meal (http://127.0.0.1:8080/meal/sandwich/<user_access_code>)
- close the open meal (http://127.0.0.1:8080/meal)
- list open/closed meals (http://127.0.0.1:8080/meal)
- list meal sandwiches (http://127.0.0.1:8080/meal/details/<meal_access_code>)

There are some requirements missing, for example: user can edit his sandwich while meal is open

### Code
My main focus was in backend side and database.

Frontend is very simple and is missing navigation.

There are some **TODOs** in code, but I wasn't able to finish them all (logging, ingredients validation when creating a sandwich)

I think the use-cases are self-explanatory and simple to follow.

Controllers are a bit messy, mainly due to the lack of time. It would be nice to add some view models, so it can be cleaner and reusable.

Only have unit tests for the following use-cases:
- Open registration for new meal
- Add sandwich to open meal
- Close open meal

Main areas are **src/app/Meal** and **src/app/User**

This two folders represent Meal and User domains.

I've used **CQRS (Command Query Responsibility Segregation)** and **DDD (Domain Driver Design)** approaches.
