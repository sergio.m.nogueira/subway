#!/bin/bash

apt update
apt install -y git
docker-php-ext-install pdo_mysql
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

cd /www/
composer install
./artisan db:ping
./artisan db:create
./artisan migrate --force
./artisan db:seed --force

php-fpm
