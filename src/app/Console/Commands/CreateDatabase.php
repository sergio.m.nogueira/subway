<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateDatabase extends Command
{
    protected $signature = 'db:create';

    protected $description = 'Create database';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $dbh = new \PDO('mysql:host=192.168.15.15;charset=utf8mb4', 'root', 'secret');

        $dbh->exec('CREATE DATABASE IF NOT EXISTS subway CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');

        return 0;
    }
}
