<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PingDB extends Command
{
    protected $signature = 'db:ping';

    protected $description = 'Test connection to database';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $ok = false;

        while (!$ok) {
            try {
                new \PDO('mysql:host=192.168.15.15;charset=utf8mb4', 'root', 'secret');
                $ok = true;
                echo 'Connection established'.PHP_EOL;
            } catch (\Throwable $exception) {
            }
        }

        return 0;
    }
}
