<?php declare(strict_types=1);

namespace App\Shared\Service;

class PHPAccessCodeGeneratorService
{
    public function generate(): string
    {
        return uniqid();
    }
}
