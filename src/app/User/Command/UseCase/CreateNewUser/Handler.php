<?php declare(strict_types=1);

namespace App\User\Command\UseCase\CreateNewUser;

use App\User\Command\Domain\Entity\UserEntity;
use App\User\Command\Domain\Exception\AccessCodeAlreadyInUseException;
use App\User\Command\Domain\Exception\InvalidUserNameException;
use App\User\Command\Domain\Exception\UnableToSaveNewUserException;
use App\User\Command\Domain\Repository\UserRepository;
use App\User\Command\Domain\Service\AccessCodeGeneratorService;
use App\User\Command\Domain\ValueObject\UserNameVO;

class Handler
{
    private AccessCodeGeneratorService $accessCodeGeneratorService;
    private UserRepository $userRepository;

    public function __construct(
        AccessCodeGeneratorService $accessCodeGeneratorService,
        UserRepository $userRepository
    ) {
        $this->accessCodeGeneratorService = $accessCodeGeneratorService;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws InvalidUserNameException
     * @throws AccessCodeAlreadyInUseException
     * @throws UnableToSaveNewUserException
     */
    public function handle(string $name): void
    {
        $user = UserEntity::newUser(
            $this->accessCodeGeneratorService->newAccessCode(),
            new UserNameVO($name)
        );

        $this->userRepository->saveNewUser($user);
    }
}
