<?php declare(strict_types=1);

namespace App\User\Command\Domain\ValueObject;

use App\User\Command\Domain\Exception\InvalidUserNameException;

class UserNameVO
{
    private const MIN_LENGTH = 3;
    private const MAX_LENGTH = 30;

    private string $name;

    /**
     * @throws InvalidUserNameException
     */
    public function __construct(string $name)
    {
        if (!$this->isValid($name)) {
            throw new InvalidUserNameException($name);
        }

        $this->name = $name;
    }

    public function value(): string
    {
        return $this->name;
    }

    private function isValid(string $name): bool
    {
        $length = mb_strlen($name);

        return $length >= self::MIN_LENGTH && $length <= self::MAX_LENGTH;
    }
}
