<?php declare(strict_types=1);

namespace App\User\Command\Domain\Entity;

use App\User\Command\Domain\ValueObject\AccessCodeVO;
use App\User\Command\Domain\ValueObject\UserNameVO;
use DateTime;

class UserEntity
{
    private int $id;
    private AccessCodeVO $accessCode;
    private UserNameVO $name;
    private DateTime $createdAt;

    public static function newUser(AccessCodeVO $accessCode, UserNameVO $name): UserEntity
    {
        $instance = new self();

        $instance->accessCode = $accessCode;
        $instance->name = $name;
        $instance->createdAt = new DateTime();

        return $instance;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function accessCode(): AccessCodeVO
    {
        return $this->accessCode;
    }

    public function name(): UserNameVO
    {
        return $this->name;
    }

    public function creationDate(): DateTime
    {
        return $this->createdAt;
    }
}
