<?php declare(strict_types=1);

namespace App\User\Command\Domain\Service;

use App\User\Command\Domain\ValueObject\AccessCodeVO;

interface AccessCodeGeneratorService
{
    public function newAccessCode(): AccessCodeVO;
}
