<?php declare(strict_types=1);

namespace App\User\Command\Domain\Exception;

class AccessCodeAlreadyInUseException extends \Exception
{
}
