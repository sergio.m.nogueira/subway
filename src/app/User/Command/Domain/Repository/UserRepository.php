<?php declare(strict_types=1);

namespace App\User\Command\Domain\Repository;

use App\User\Command\Domain\Entity\UserEntity;
use App\User\Command\Domain\Exception\AccessCodeAlreadyInUseException;
use App\User\Command\Domain\Exception\UnableToSaveNewUserException;

interface UserRepository
{
    /**
     * @throws AccessCodeAlreadyInUseException
     * @throws UnableToSaveNewUserException
     */
    public function saveNewUser(UserEntity $user): void;
}
