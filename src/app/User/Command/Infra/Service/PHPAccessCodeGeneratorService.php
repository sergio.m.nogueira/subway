<?php declare(strict_types=1);

namespace App\User\Command\Infra\Service;

use App\Shared\Service\PHPAccessCodeGeneratorService as SharedPHPAccessCodeGeneratorService;
use App\User\Command\Domain\Service\AccessCodeGeneratorService;
use App\User\Command\Domain\ValueObject\AccessCodeVO;

class PHPAccessCodeGeneratorService
    extends SharedPHPAccessCodeGeneratorService
    implements AccessCodeGeneratorService
{
    public function newAccessCode(): AccessCodeVO
    {
        return new AccessCodeVO($this->generate());
    }
}
