<?php declare(strict_types=1);

namespace App\User\Command\Infra\Repository\UserRepository;

use App\User\Command\Domain\Entity\UserEntity;
use App\User\Command\Domain\Exception\AccessCodeAlreadyInUseException;
use App\User\Command\Domain\Exception\UnableToSaveNewUserException;
use App\User\Command\Domain\Repository\UserRepository;
use Illuminate\Support\Facades\DB;

class MySql implements UserRepository
{
    private const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * @throws AccessCodeAlreadyInUseException
     * @throws UnableToSaveNewUserException
     */
    public function saveNewUser(UserEntity $user): void
    {
        try {
            DB::insert(
                'insert into users (name, access_code, created_at) values (?, ?, ?)',
                [
                    $user->name()->value(),
                    $user->accessCode()->value(),
                    $user->creationDate()->format(self::DATE_TIME_FORMAT),
                ]
            );
        } catch (\Throwable $exception) {
            if ((int) $exception->getCode() === 23000) {
                throw new AccessCodeAlreadyInUseException($user->accessCode());
            } else {
                // Log error
                throw new UnableToSaveNewUserException();
            }
        }
    }
}
