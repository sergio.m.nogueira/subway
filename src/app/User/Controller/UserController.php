<?php declare(strict_types=1);

namespace App\User\Controller;

use App\Http\Controllers\Controller;
use App\User\Command\Domain\Exception\AccessCodeAlreadyInUseException;
use App\User\Command\Domain\Exception\InvalidUserNameException;
use App\User\Command\Domain\Exception\UnableToSaveNewUserException;
use App\User\Command\UseCase\CreateNewUser\Handler as CreateNewUserCommandHandler;
use App\User\Query\UseCase\FetchUsersAccessCodes\Handler as FetchUsersAccessCodesQueryHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    private CreateNewUserCommandHandler $createNewUserCommandHandler;
    private FetchUsersAccessCodesQueryHandler $fetchUsersAccessCodesQueryHandler;

    public function __construct(
        CreateNewUserCommandHandler $createNewUserCommandHandler,
        FetchUsersAccessCodesQueryHandler $fetchUsersAccessCodesQueryHandler
    ) {
        $this->createNewUserCommandHandler = $createNewUserCommandHandler;
        $this->fetchUsersAccessCodesQueryHandler = $fetchUsersAccessCodesQueryHandler;
    }

    public function __invoke(Request $request): Response
    {
        if ($request->method() === 'POST') {
            $data['saveError'] = $this->addUser($request);
        }

        $data['users'] = $this->listUsers();
        return response()->view('users', $data);
    }

    private function listUsers(): array
    {
        return $this->fetchUsersAccessCodesQueryHandler->handle();
    }

    private function addUser(Request $request): ?string
    {
        $error = null;

        try {
            $this->createNewUserCommandHandler->handle((string) $request->post('name'));
        } catch (InvalidUserNameException $exception) {
            $error = sprintf('Name: %s is not valid', $exception->getMessage()); // This could be a translation key, so it can be translated later
        } catch (AccessCodeAlreadyInUseException $exception) {
            $error = sprintf('Access code: %s is already in user', $exception->getMessage());
        } catch (UnableToSaveNewUserException) {
            // Log error
            $error = 'An error occurred while creating new user';
        }

        return $error;
    }
}
