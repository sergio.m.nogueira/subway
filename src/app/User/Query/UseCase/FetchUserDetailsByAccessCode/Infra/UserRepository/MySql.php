<?php declare(strict_types=1);

namespace App\User\Query\UseCase\FetchUserDetailsByAccessCode\Infra\UserRepository;

use App\User\Query\UseCase\FetchUserDetailsByAccessCode\UserRepository;
use Illuminate\Support\Facades\DB;

class MySql implements UserRepository
{
    public function fetchUserDetailsByAccessCode(string $accessCode): array
    {
        $user = DB::selectOne('select * from users where access_code = ?', [$accessCode]);

        return $user
            ? [
                'id' => $user->id,
                'name' => $user->name,
                'accessCode' => $user->access_code,
                'created_at' => $user->created_at
            ]
            : [];
    }
}
