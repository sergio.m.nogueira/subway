<?php declare(strict_types=1);

namespace App\User\Query\UseCase\FetchUserDetailsByAccessCode;

interface UserRepository
{
    public function fetchUserDetailsByAccessCode(string $accessCode): array;
}
