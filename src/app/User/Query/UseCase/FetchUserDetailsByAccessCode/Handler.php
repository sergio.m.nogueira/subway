<?php declare(strict_types=1);

namespace App\User\Query\UseCase\FetchUserDetailsByAccessCode;

class Handler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(string $accessCode): array
    {
        return $this->userRepository->fetchUserDetailsByAccessCode($accessCode);
    }
}
