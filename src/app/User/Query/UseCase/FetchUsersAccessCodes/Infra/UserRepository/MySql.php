<?php declare(strict_types=1);

namespace App\User\Query\UseCase\FetchUsersAccessCodes\Infra\UserRepository;

use App\User\Query\UseCase\FetchUsersAccessCodes\UserRepository as RepositoryInterface;
use Illuminate\Support\Facades\DB;

class MySql implements RepositoryInterface
{
    public function fetchAllUsersAccessCodes(): array
    {
        $users = DB::select('select name, access_code from users');

        return array_map(
            function (\stdClass $result) {
                return [
                    'name' => $result->name,
                    'accessCode' => $result->access_code,
                ];
            },
            $users
        );
    }
}
