<?php declare(strict_types=1);

namespace App\User\Query\UseCase\FetchUsersAccessCodes;

interface UserRepository
{
    public function fetchAllUsersAccessCodes(): array;
}
