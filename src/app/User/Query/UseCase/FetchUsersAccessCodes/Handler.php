<?php declare(strict_types=1);

namespace App\User\Query\UseCase\FetchUsersAccessCodes;

class Handler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(): array
    {
        return $this->userRepository->fetchAllUsersAccessCodes();
    }
}
