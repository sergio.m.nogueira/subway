<?php declare(strict_types=1);

namespace App\User\Query\UseCase\FetchUsersNamesByIds;

interface UsersRepository
{
    public function fetchUsersNamesByIds(array $ids): array;
}
