<?php declare(strict_types=1);

namespace App\User\Query\UseCase\FetchUsersNamesByIds;

class Handler
{
    private UsersRepository $usersRepository;

    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    public function handle(array $ids): array
    {
        return $this->usersRepository->fetchUsersNamesByIds($ids);
    }
}
