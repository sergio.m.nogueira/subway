<?php declare(strict_types=1);

namespace App\User\Query\UseCase\FetchUsersNamesByIds\Infra\UsersRepository;

use App\User\Query\UseCase\FetchUsersNamesByIds\UsersRepository;
use Illuminate\Support\Facades\DB;

class MySql implements UsersRepository
{
    public function fetchUsersNamesByIds(array $ids): array
    {
        $ids = implode(',', $ids);

        $query = <<<SQL
SELECT id, name
FROM users
WHERE id IN ({$ids})
SQL;

        $users = [];

        foreach (DB::select($query) as $user) {
            $users[$user->id] = $user->name;
        }

        return $users;
    }
}
