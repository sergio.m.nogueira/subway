<?php declare(strict_types=1);

namespace App\Providers;

use App\Meal\Command\Infra\Repository\MealRepository\MySql as MySqlMealRepository;
use App\Meal\Command\Infra\Repository\UserRepository\UserDomainCall;
use App\Meal\Command\Infra\Service\PHPAccessCodeGeneratorService;
use App\Meal\Command\UseCase\AddSandwichToMeal\Handler as AddSandwichToMealHandler;
use App\Meal\Command\UseCase\CloseOpenMeal\Handler as CloseOpenMealHandler;
use App\Meal\Command\UseCase\OpenRegistrationForNewMeal\Handler as OpenRegistrationForNewMealHandler;
use App\Meal\Query\UseCase\FetchAllIngredients\Handler as FetchAllIngredientsHandler;
use App\Meal\Query\UseCase\FetchAllIngredients\Infra\IngredientsRepository\MySql as FetchAllIngredientsMySqlRepository;
use App\Meal\Query\UseCase\FetchAllMeals\Handler as FetchAllMealsHandler;
use App\Meal\Query\UseCase\FetchAllMeals\Infra\MealRepository\MySql as FetchAllMealsMySqlRepository;
use App\Meal\Query\UseCase\FetchMealDetails\Handler as FetchOpenMealDetailsHandler;
use App\Meal\Query\UseCase\FetchMealDetails\Infra\MealRepository\MySql as FetchOpenMealDetailsMySqlRepository;
use Illuminate\Support\ServiceProvider;

class MealServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register()
    {
        // Meal
        $this->app->singleton(
            OpenRegistrationForNewMealHandler::class, function ($app) {
            return new OpenRegistrationForNewMealHandler(
                $app->make(MySqlMealRepository::class),
                $app->make(PHPAccessCodeGeneratorService::class)
            );
        });

        $this->app->singleton(
            CloseOpenMealHandler::class, function ($app) {
            return new CloseOpenMealHandler(
                $app->make(MySqlMealRepository::class)
            );
        });

        $this->app->singleton(
            FetchOpenMealDetailsHandler::class, function ($app) {
            return new FetchOpenMealDetailsHandler(
                $app->make(FetchOpenMealDetailsMySqlRepository::class)
            );
        });

        $this->app->singleton(
            FetchAllMealsHandler::class, function ($app) {
            return new FetchAllMealsHandler(
                $app->make(FetchAllMealsMySqlRepository::class)
            );
        });


        // Sandwich
        $this->app->singleton(
            AddSandwichToMealHandler::class, function ($app) {
            return new AddSandwichToMealHandler(
                $app->make(MySqlMealRepository::class),
                $app->make(UserDomainCall::class)
            );
        });

        $this->app->singleton(
            FetchAllIngredientsHandler::class, function ($app) {
            return new FetchAllIngredientsHandler(
                $app->make(FetchAllIngredientsMySqlRepository::class)
            );
        });
    }

    /**
     * @return void
     */
    public function boot()
    {
        //
    }
}
