<?php declare(strict_types=1);

namespace App\Providers;

use App\User\Command\Infra\Repository\UserRepository\MySql as MySqlUserRepository;
use App\User\Command\Infra\Service\PHPAccessCodeGeneratorService;
use App\User\Command\UseCase\CreateNewUser\Handler as CreateNewUserHandler;
use App\User\Query\UseCase\FetchUserDetailsByAccessCode\Handler as FetchUserDetailsByAccessCodeHandler;
use App\User\Query\UseCase\FetchUserDetailsByAccessCode\Infra\UserRepository\MySql as FetchUserDetailsByAccessCodeMySqlRepository;
use App\User\Query\UseCase\FetchUsersAccessCodes\Handler as FetchUsersAccessCodesHandler;
use App\User\Query\UseCase\FetchUsersAccessCodes\Infra\UserRepository\MySql as FetchUsersAccessCodesMySqlRepository;
use App\User\Query\UseCase\FetchUsersNamesByIds\Handler as FetchUsersNamesByIdsHandler;
use App\User\Query\UseCase\FetchUsersNamesByIds\Infra\UsersRepository\MySql as FetchUsersNamesByIdsMySqlRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Service provider for User domain
 */
class UserServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            CreateNewUserHandler::class, function ($app) {
            return new CreateNewUserHandler(
                $app->make(PHPAccessCodeGeneratorService::class),
                $app->make(MySqlUserRepository::class)
            );
        });

        $this->app->singleton(
            FetchUserDetailsByAccessCodeHandler::class, function ($app) {
            return new FetchUserDetailsByAccessCodeHandler(
                $app->make(FetchUserDetailsByAccessCodeMySqlRepository::class)
            );
        });

        $this->app->singleton(
            FetchUsersAccessCodesHandler::class, function ($app) {
            return new FetchUsersAccessCodesHandler(
                $app->make(FetchUsersAccessCodesMySqlRepository::class)
            );
        });

        $this->app->singleton(
            FetchUsersNamesByIdsHandler::class, function ($app) {
            return new FetchUsersNamesByIdsHandler(
                $app->make(FetchUsersNamesByIdsMySqlRepository::class)
            );
        });
    }

    /**
     * @return void
     */
    public function boot()
    {
        //
    }
}
