<?php declare(strict_types=1);

namespace App\Meal\Controller;

use App\Http\Controllers\Controller;
use App\Meal\Query\UseCase\FetchMealDetails\Handler as FetchMealDetailsQueryHandler;
use Illuminate\Http\Response;

class MealDetailsController extends Controller
{
    private FetchMealDetailsQueryHandler $fetchMealDetailsQueryHandler;

    public function __construct(FetchMealDetailsQueryHandler $fetchMealDetailsQueryHandler)
    {
        $this->fetchMealDetailsQueryHandler = $fetchMealDetailsQueryHandler;
    }

    public function __invoke(string $mealAccessCode): Response
    {
        $mealDetails = $this->fetchMealDetailsQueryHandler->handle($mealAccessCode);

        return response()->view(
            'meal_details',
            [
                'mealAccessCode' => $mealAccessCode,
                'mealDetails' => $mealDetails,
            ]
        );
    }
}
