<?php declare(strict_types=1);

namespace App\Meal\Controller;

use App\Http\Controllers\Controller;
use App\Meal\Command\Domain\Exception\AMealIsAlreadyOpenException;
use App\Meal\Command\UseCase\CloseOpenMeal\Handler as CloseOpenMealCommandHandler;
use App\Meal\Command\UseCase\OpenRegistrationForNewMeal\Handler as OpenRegistrationForNewMealCommandHandler;
use App\Meal\Query\UseCase\FetchAllMeals\Handler as FetchAllMealsQueryHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class MealController extends Controller
{
    private OpenRegistrationForNewMealCommandHandler $openRegistrationForNewMealCommandHandler;
    private FetchAllMealsQueryHandler $fetchAllMealsQueryHandler;
    private CloseOpenMealCommandHandler $closeOpenMealCommandHandler;

    public function __construct(
        OpenRegistrationForNewMealCommandHandler $openRegistrationForNewMealCommandHandler,
        FetchAllMealsQueryHandler $fetchAllMealsQueryHandler,
        CloseOpenMealCommandHandler $closeOpenMealCommandHandler
    ) {
        $this->openRegistrationForNewMealCommandHandler = $openRegistrationForNewMealCommandHandler;
        $this->fetchAllMealsQueryHandler = $fetchAllMealsQueryHandler;
        $this->closeOpenMealCommandHandler = $closeOpenMealCommandHandler;
    }

    public function __invoke(Request $request): Response
    {
        $action = $request->query('action');

        switch ($action) {
            case 'open':
                return $this->openRegistrationForANewMeal();
            case 'close':
                return $this->closeOpenMeal();
        }

        return $this->listMeals();
    }

    private function listMeals(): Response
    {
        list('open' => $openMeal, 'closed' => $closedMeals) = $this->fetchMealsHistory();

        return response()->view(
            'meal',
            [
                'openMeal' => $openMeal,
                'closedMeals' => $closedMeals,
            ]
        );
    }

    private function fetchMealsHistory(): array
    {
        $open = [];
        $closed = [];

        foreach ($this->fetchAllMealsQueryHandler->handle() as $meal) {
            if ($meal['status'] === 'open') {
                $open = $meal;
            } else {
                $closed[] = $meal;
            }
        }

        return [
            'open' => $open,
            'closed' => $closed,
        ];
    }

    private function openRegistrationForANewMeal(): Response
    {
        $templateName = 'meal';

        try {
            $accessCode = $this->openRegistrationForNewMealCommandHandler->open();
            $templateName = 'meal_creation_success';
            $data['accessCode'] = $accessCode;
        } catch(AMealIsAlreadyOpenException) {
            $data['error'] = 'A meal is already open'; // This could be a translation key, so it can be translated later
        } catch (Throwable) {
            // TODO: Add logging here
            $data['error'] = 'An error occurred while trying to open the meal';
        }

        return response()->view($templateName, $data);
    }

    private function closeOpenMeal(): Response
    {
        $this->closeOpenMealCommandHandler->handle();

        return response()->redirectTo('/meal');
    }
}
