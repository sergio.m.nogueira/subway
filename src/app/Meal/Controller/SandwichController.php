<?php declare(strict_types=1);

namespace App\Meal\Controller;

use App\Http\Controllers\Controller;
use App\Meal\Command\Domain\Exception\MealIsClosedException;
use App\Meal\Command\Domain\Exception\UserAlreadyHasSandwichInTheMealException;
use App\Meal\Command\UseCase\AddSandwichToMeal\Command as AddSandwichToMealCommand;
use App\Meal\Command\UseCase\AddSandwichToMeal\Handler as AddSandwichToMealCommandHandler;
use App\Meal\Query\UseCase\FetchAllIngredients\Handler as FetchAllIngredientsQueryHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class SandwichController extends Controller
{
    private FetchAllIngredientsQueryHandler $fetchAllIngredientsQueryHandler;
    private AddSandwichToMealCommandHandler $addSandwichToMealCommandHandler;

    public function __construct(
        FetchAllIngredientsQueryHandler $fetchAllIngredientsQueryHandler,
        AddSandwichToMealCommandHandler $addSandwichToMealCommandHandler
    ) {
        $this->fetchAllIngredientsQueryHandler = $fetchAllIngredientsQueryHandler;
        $this->addSandwichToMealCommandHandler = $addSandwichToMealCommandHandler;
    }

    public function __invoke(string $userAccessCode,  Request $request): Response
    {
        $data = [
            'userAccessCode' => $userAccessCode,
        ];

        if ($request->method() === 'POST') {
            return $this->saveSandwich($userAccessCode, $request);
        }

        // TODO: Check if user already has ordered a sandwich, and if it does and meal is still open, edit sandwich

        $data = array_merge($data, $this->getSandwichComponents());
        return response()->view('meal_sandwich_form', $data);
    }

    private function saveSandwich(string $userAccessCode,  Request $request): Response
    {
        $errorMsg = '';

        try {
            $command = new AddSandwichToMealCommand(
                $userAccessCode,
                (int) $request->post('type'),
                (int) $request->post('size'),
                $request->post('oven-baked') === 'on',
                (int) $request->post('taste'),
                $request->post('extras', []),
                $request->post('vegetables', []),
                (int) $request->post('sauce')
            );

            $this->addSandwichToMealCommandHandler->handle($command);
        } catch (UserAlreadyHasSandwichInTheMealException) {
            $errorMsg = 'Only one sandwich per user is allowed';
        } catch (MealIsClosedException) {
            $errorMsg = 'There is no open meal at the moment';
        } catch (Throwable $exception) {
            $errorMsg = 'An error occurred while trying to save your order, please try again';
        }

        return $errorMsg === ''
            ? response()->view('meal_sandwich_success')
            : response()->view('meal_sandwich_error', compact('errorMsg'));
    }

    private function getSandwichComponents(): array
    {
        $sandwichComponents = $this->fetchAllIngredientsQueryHandler->handle();

        return [
            'types' => $sandwichComponents['bread_type'],
            'tastes' => $sandwichComponents['taste'],
            'extras' => $sandwichComponents['extra'],
            'vegetables' => $sandwichComponents['vegetable'],
            'sauces' => $sandwichComponents['sauce'],
        ];
    }
}
