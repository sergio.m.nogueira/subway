<?php declare(strict_types=1);

namespace App\Meal\Query\UseCase\FetchAllMeals;

interface MealRepository
{
    public function fetchAll(): array;
}
