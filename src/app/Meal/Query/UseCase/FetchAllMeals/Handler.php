<?php declare(strict_types=1);

namespace App\Meal\Query\UseCase\FetchAllMeals;

class Handler
{
    private MealRepository $mealRepository;

    public function __construct(MealRepository $mealRepository)
    {
        $this->mealRepository = $mealRepository;
    }

    public function handle(): array
    {
        return $this->mealRepository->fetchAll();
    }
}
