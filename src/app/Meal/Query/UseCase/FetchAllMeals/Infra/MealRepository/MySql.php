<?php declare(strict_types=1);

namespace App\Meal\Query\UseCase\FetchAllMeals\Infra\MealRepository;

use App\Meal\Query\UseCase\FetchAllMeals\MealRepository;
use Illuminate\Support\Facades\DB;

class MySql implements MealRepository
{
    public function fetchAll(): array
    {
        $result = DB::select('SELECT access_code, status, created_at, closed_at FROM meals ORDER BY created_at DESC');

        return array_map(
            function (\stdClass $row) {
                return [
                    'accessCode' => $row->access_code,
                    'status' => $row->status === 1 ? 'open' : 'closed',
                    'createdAt' => $row->created_at,
                    'closedAt' => $row->closed_at,
                ];
            },
            $result
        );
    }
}
