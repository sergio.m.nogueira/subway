<?php declare(strict_types=1);

namespace App\Meal\Query\UseCase\FetchAllIngredients\Infra\IngredientsRepository;

use App\Meal\Query\UseCase\FetchAllIngredients\IngredientsRepository;
use Illuminate\Support\Facades\DB;

class MySql implements IngredientsRepository
{
    public function fetchAllIngredients(): array
    {
        $groupedResults = [];

        $results = DB::select('SELECT * FROM sandwich_ingredients');
        foreach ($results as $result) {
            $groupedResults[$result->type][] = [
                'id' => $result->id,
                'name' => $result->name,
            ];
        }

        return $groupedResults;
    }
}
