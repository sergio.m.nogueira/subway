<?php declare(strict_types=1);

namespace App\Meal\Query\UseCase\FetchAllIngredients;

class Handler
{
    private IngredientsRepository $ingredientsRepository;

    public function __construct(IngredientsRepository $ingredientsRepository)
    {
        $this->ingredientsRepository = $ingredientsRepository;
    }

    public function handle(): array
    {
        return $this->ingredientsRepository->fetchAllIngredients();
    }
}
