<?php declare(strict_types=1);

namespace App\Meal\Query\UseCase\FetchAllIngredients;

interface IngredientsRepository
{
    public function fetchAllIngredients(): array;
}
