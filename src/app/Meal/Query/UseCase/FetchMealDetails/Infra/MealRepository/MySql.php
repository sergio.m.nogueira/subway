<?php declare(strict_types=1);

namespace App\Meal\Query\UseCase\FetchMealDetails\Infra\MealRepository;

use App\Meal\Query\UseCase\FetchMealDetails\MealRepository;
use App\User\Query\UseCase\FetchUsersNamesByIds\Handler as FetchUsersNamesByIdsQueryHandler;
use Illuminate\Support\Facades\DB;

class MySql implements MealRepository
{
    private FetchUsersNamesByIdsQueryHandler $fetchUsersNamesByIdsHandler;

    private array $usersNames = [];
    private array $ingredientsList = [];

    public function __construct(FetchUsersNamesByIdsQueryHandler $fetchUsersNamesByIdsHandler)
    {
        $this->fetchUsersNamesByIdsHandler = $fetchUsersNamesByIdsHandler;
    }

    public function fetchMealDetails(string $accessCode): array
    {
        $query = <<<SQL
SELECT ms.*
FROM meals m
JOIN meal_sandwich ms on m.id = ms.meal_id
WHERE m.access_code = '{$accessCode}'
SQL;

        $sandwiches = DB::select($query);

        if (!$sandwiches) {
            return [];
        }

        // Extract all users ids to get all user names in the same request
        $this->usersNames = $this->fetchUsersNamesByIdsHandler->handle(array_column($sandwiches, 'user_id'));

        // Fetch all ingredients in the same query (prevents multiple DB requests)
        $this->ingredientsList = $this->fetchAllIngredients();

        $result = [];
        foreach ($sandwiches as $sandwich) {
            $result[] = $this->buildSandwichDetails($sandwich);
        }

        return $result;
    }

    private function fetchAllIngredients(): array
    {
        $result = [];

        foreach (DB::select('SELECT * FROM sandwich_ingredients') as $ingredient) {
            $result[$ingredient->id] = $ingredient->name;
        }

        return $result;
    }

    private function buildSandwichDetails(\stdClass $sandwich): array
    {
        $characteristics = json_decode($sandwich->characteristics, true);
        $ingredients = json_decode($sandwich->ingredients, true);

        return [
            'user' => $this->usersNames[$sandwich->user_id],
            'size' => $characteristics['size'],
            'ovenBaked' => $characteristics['ovenBaked'] === 1 ? 'yes' : 'no', // TODO: This is view responsibility
            'breadType' => $this->ingredientsList[$ingredients['bread']],
            'sauce' => (int) $ingredients['sauce'] !== 0 ? $this->ingredientsList[$ingredients['sauce']] : 'no sauce', // TODO: This is view responsibility
            'extras' => $this->mapExtras($ingredients['extras']),
            'vegetables' => $this->mapVegetables($ingredients['vegetables']),
        ];
    }

    private function mapExtras(array $sandwichExtras): string
    {
        $extras = [];
        foreach ($sandwichExtras as $extra) {
            $extras[] = $this->ingredientsList[$extra];
        }

        return implode(', ', $extras);
    }

    private function mapVegetables(array $sandwichVegetables): string
    {
        $vegetables = [];
        foreach ($sandwichVegetables as $vegetable) {
            $vegetables[] = $this->ingredientsList[$vegetable];
        }

        return implode(', ', $vegetables);
    }
}
