<?php declare(strict_types=1);

namespace App\Meal\Query\UseCase\FetchMealDetails;

interface MealRepository
{
    public function fetchMealDetails(string $accessCode): array;
}
