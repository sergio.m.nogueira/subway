<?php declare(strict_types=1);

namespace App\Meal\Query\UseCase\FetchMealDetails;

class Handler
{
    private MealRepository $mealRepository;

    public function __construct(MealRepository $mealRepository)
    {
        $this->mealRepository = $mealRepository;
    }

    public function handle(string $accessCode): array
    {
        return $this->mealRepository->fetchMealDetails($accessCode);
    }
}
