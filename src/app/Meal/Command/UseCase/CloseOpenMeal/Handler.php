<?php declare(strict_types=1);

namespace App\Meal\Command\UseCase\CloseOpenMeal;

use App\Meal\Command\Domain\Exception\AnErrorOccurredWhileTryingToCloseMealException;
use App\Meal\Command\Domain\Exception\NoOpenMealException;
use App\Meal\Command\Domain\Repository\MealRepository;

class Handler
{
    private MealRepository $mealRepository;

    public function __construct(MealRepository $mealRepository)
    {
        $this->mealRepository = $mealRepository;
    }

    /**
     * @throws NoOpenMealException
     * @throws AnErrorOccurredWhileTryingToCloseMealException
     */
    public function handle(): void
    {
        $meal = $this->mealRepository->fetchOpenMeal();
        if (!$meal) {
            throw new NoOpenMealException();
        }

        $meal->close();

        $this->mealRepository->saveClosedMeal($meal);
    }
}
