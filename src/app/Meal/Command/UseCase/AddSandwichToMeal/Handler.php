<?php declare(strict_types=1);

namespace App\Meal\Command\UseCase\AddSandwichToMeal;

use App\Meal\Command\Domain\Exception\InvalidAccessCodeException;
use App\Meal\Command\Domain\Exception\MealIsClosedException;
use App\Meal\Command\Domain\Exception\UnableToSaveNewSandwichException;
use App\Meal\Command\Domain\Exception\UserAlreadyHasSandwichInTheMealException;
use App\Meal\Command\Domain\Repository\MealRepository;
use App\Meal\Command\Domain\Repository\UserRepository;

class Handler
{
    private MealRepository $mealRepository;
    private UserRepository $userRepository;

    public function __construct(
        MealRepository $mealRepository,
        UserRepository $userRepository
    ) {
        $this->mealRepository = $mealRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @throws InvalidAccessCodeException
     * @throws MealIsClosedException
     * @throws UnableToSaveNewSandwichException
     * @throws UserAlreadyHasSandwichInTheMealException
     */
    public function handle(Command $command): void
    {
        $meal = $this->mealRepository->fetchOpenMeal();
        if (!$meal) {
            throw new MealIsClosedException();
        }

        $userId = $this->userRepository->fetchUserIdByAccessToken($command->userAccessCode());
        if (!$userId) {
            throw new InvalidAccessCodeException($command->userAccessCode());
        }

        // TODO: Missing fields validation


        $meal->addNewSandwich(
            $userId,
            $command->type(),
            $command->size(),
            $command->ovenBaked(),
            $command->taste(),
            $command->extras(),
            $command->vegetables(),
            $command->sauce()
        );

        $this->mealRepository->saveMealSandwiches($meal);
    }
}
