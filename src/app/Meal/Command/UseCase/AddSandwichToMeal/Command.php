<?php declare(strict_types=1);

namespace App\Meal\Command\UseCase\AddSandwichToMeal;

class Command
{
    private int $type;
    private string $userAccessCode;
    private int $size;
    private bool $ovenBaked;
    private int $taste;
    private array $extras;
    private array $vegetables;
    private int $sauce;

    public function __construct(
        string $userAccessCode,
        int $type,
        int $size,
        bool $ovenBaked,
        int $taste,
        array $extras,
        array $vegetables,
        int $sauce
    ) {
        $this->userAccessCode = $userAccessCode;
        $this->type = $type;
        $this->size = $size;
        $this->ovenBaked = $ovenBaked;
        $this->taste = $taste;
        $this->extras = $extras;
        $this->vegetables = $vegetables;
        $this->sauce = $sauce;
    }

    public function userAccessCode(): string
    {
        return $this->userAccessCode;
    }

    public function type(): int
    {
        return $this->type;
    }

    public function size(): int
    {
        return $this->size;
    }

    public function ovenBaked(): bool
    {
        return $this->ovenBaked;
    }

    public function taste(): int
    {
        return $this->taste;
    }

    public function extras(): array
    {
        return $this->extras;
    }

    public function vegetables(): array
    {
        return $this->vegetables;
    }

    public function sauce(): int
    {
        return $this->sauce;
    }
}
