<?php declare(strict_types=1);

namespace App\Meal\Command\UseCase\OpenRegistrationForNewMeal;

use App\Meal\Command\Domain\Entity\MealEntity;
use App\Meal\Command\Domain\Exception\AMealIsAlreadyOpenException;
use App\Meal\Command\Domain\Repository\MealRepository;
use App\Meal\Command\Domain\Service\AccessCodeGeneratorService;

class Handler
{
    private MealRepository $mealRepository;
    private AccessCodeGeneratorService $accessCodeGeneratorService;

    public function __construct(
        MealRepository $mealRepository,
        AccessCodeGeneratorService $accessCodeGeneratorService
    ) {
        $this->mealRepository = $mealRepository;
        $this->accessCodeGeneratorService = $accessCodeGeneratorService;
    }

    /**
     * @return string Access code
     * @throws AMealIsAlreadyOpenException
     */
    public function open(): string
    {
        if ($this->hasOpenMeal()) {
            throw new AMealIsAlreadyOpenException();
        }

        $newMeal = $this->createNewMeal();

        $this->mealRepository->saveNewMeal($newMeal);

        return $newMeal->accessCode()->value();
    }

    private function hasOpenMeal(): bool
    {
        return $this->mealRepository->fetchOpenMeal() !== null;
    }

    private function createNewMeal(): MealEntity
    {
        return MealEntity::openNewMeal($this->accessCodeGeneratorService->newAccessCode());
    }
}
