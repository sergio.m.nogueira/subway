<?php declare(strict_types=1);

namespace App\Meal\Command\Domain\ValueObject;

use App\Meal\Command\Domain\Exception\InvalidAccessCodeException;

class AccessCodeVO
{
    private const MIN_LENGTH = 3;
    private const MAX_LENGTH = 30;

    private string $accessCode;

    /**
     * @throws InvalidAccessCodeException
     */
    public function __construct(string $accessCode)
    {
        if (!$this->isValid($accessCode)) {
            throw new InvalidAccessCodeException($accessCode);
        }

        $this->accessCode = $accessCode;
    }

    public function value(): string
    {
        return $this->accessCode;
    }

    private function isValid(string $accessCode): bool
    {
        $length = mb_strlen($accessCode);

        return $length >= self::MIN_LENGTH && $length <= self::MAX_LENGTH;
    }
}
