<?php declare(strict_types=1);

namespace App\Meal\Command\Domain\Entity;

use App\Meal\Command\Domain\ValueObject\AccessCodeVO;
use DateTime;

class MealEntity
{
    private const STATUS_OPEN = 1;
    private const STATUS_CLOSED = 0;

    private ?int $id;
    private int $status;
    private ?DateTime $openedAt = null;
    private ?DateTime $closedAt = null;
    private AccessCodeVO $accessCode;
    /** @var SandwichEntity[] */
    private array $newSandwiches = [];

    public static function openNewMeal(AccessCodeVO $accessCode): self
    {
        $instance = new self();

        $instance->status = self::STATUS_OPEN;
        $instance->openedAt = new DateTime();
        $instance->accessCode = $accessCode;

        return $instance;
    }

    public function addNewSandwich(
        int $userId,
        int $breadType,
        int $size,
        bool $ovenBaked,
        int $taste,
        array $extras,
        array $vegetables,
        int $sauce
    ): self
    {
        $this->newSandwiches[] = new SandwichEntity(
            $userId,
            $breadType,
            $size,
            $ovenBaked,
            $taste,
            $extras,
            $vegetables,
            $sauce
        );

        return $this;
    }

    public function close(): self
    {
        $this->status = self::STATUS_CLOSED;
        $this->closedAt = new DateTime();

        return $this;
    }

    public function isOpen(): bool
    {
        return $this->status === self::STATUS_OPEN;
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function accessCode(): AccessCodeVO
    {
        return $this->accessCode;
    }

    public function openDate(): DateTime
    {
        return $this->openedAt;
    }

    public function closeDate(): ?DateTime
    {
        return $this->closedAt;
    }

    /** @return SandwichEntity[] */
    public function newSandwiches(): array
    {
        return $this->newSandwiches;
    }

    private function __construct() {}
}
