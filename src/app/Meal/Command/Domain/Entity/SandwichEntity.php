<?php declare(strict_types=1);

namespace App\Meal\Command\Domain\Entity;

use DateTime;

class SandwichEntity
{
    private int $userId;
    private int $breadType;
    private int $size;
    private bool $ovenBaked;
    private int $taste;
    private array $extras;
    private array $vegetables;
    private int $sauce;
    private DateTime $createdAt;

    public function __construct(
        int $userId,
        int $breadType,
        int $size,
        bool $ovenBaked,
        int $taste,
        array $extras,
        array $vegetables,
        int $sauce
    ) {
        $this->userId = $userId;
        $this->breadType = $breadType;
        $this->size = $size;
        $this->ovenBaked = $ovenBaked;
        $this->taste = $taste;
        $this->extras = $extras;
        $this->vegetables = $vegetables;
        $this->sauce = $sauce;
        $this->createdAt = new DateTime();
    }

    public function userId(): int
    {
        return $this->userId;
    }

    public function breadType(): int
    {
        return $this->breadType;
    }

    public function size(): int
    {
        return $this->size;
    }

    public function isOvenBaked(): bool
    {
        return $this->ovenBaked;
    }

    public function taste(): int
    {
        return $this->taste;
    }

    public function extras(): array
    {
        return $this->extras;
    }

    public function vegetables(): array
    {
        return $this->vegetables;
    }

    public function sauce(): int
    {
        return $this->sauce;
    }

    public function createdAt(): DateTime
    {
        return $this->createdAt;
    }
}
