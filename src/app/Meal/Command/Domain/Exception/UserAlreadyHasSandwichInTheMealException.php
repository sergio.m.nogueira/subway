<?php declare(strict_types=1);

namespace App\Meal\Command\Domain\Exception;

class UserAlreadyHasSandwichInTheMealException extends \Exception
{
}
