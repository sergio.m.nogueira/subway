<?php declare(strict_types=1);

namespace App\Meal\Command\Domain\Exception;

class AnErrorOccurredWhileTryingToCloseMealException extends \Exception
{
}
