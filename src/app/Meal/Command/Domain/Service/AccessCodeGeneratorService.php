<?php declare(strict_types=1);

namespace App\Meal\Command\Domain\Service;

use App\Meal\Command\Domain\ValueObject\AccessCodeVO;

interface AccessCodeGeneratorService
{
    public function newAccessCode(): AccessCodeVO;
}
