<?php declare(strict_types=1);

namespace App\Meal\Command\Domain\Repository;

interface UserRepository
{
    public function fetchUserIdByAccessToken(string $accessToken): ?int;
}
