<?php declare(strict_types=1);

namespace App\Meal\Command\Domain\Repository;

use App\Meal\Command\Domain\Entity\MealEntity;
use App\Meal\Command\Domain\Exception\AccessCodeAlreadyInUseException;
use App\Meal\Command\Domain\Exception\AnErrorOccurredWhileTryingToCloseMealException;
use App\Meal\Command\Domain\Exception\UnableToOpenMealRegistrationException;
use App\Meal\Command\Domain\Exception\UnableToSaveNewSandwichException;
use App\Meal\Command\Domain\Exception\UserAlreadyHasSandwichInTheMealException;

interface MealRepository
{
    public function fetchOpenMeal(): ?MealEntity;

    /**
     * @throws AccessCodeAlreadyInUseException
     * @throws UnableToOpenMealRegistrationException
     */
    public function saveNewMeal(MealEntity $meal): void;

    /**
     * @throws UnableToSaveNewSandwichException
     * @throws UserAlreadyHasSandwichInTheMealException
     */
    public function saveMealSandwiches(MealEntity $meal): void;

    /**
     * @throws AnErrorOccurredWhileTryingToCloseMealException
     */
    public function saveClosedMeal(MealEntity $meal): void;
}
