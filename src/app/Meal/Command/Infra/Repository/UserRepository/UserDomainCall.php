<?php declare(strict_types=1);

namespace App\Meal\Command\Infra\Repository\UserRepository;

use App\Meal\Command\Domain\Repository\UserRepository;
use App\User\Query\UseCase\FetchUserDetailsByAccessCode\Handler as FetchUserDetailsByAccessCodeQueryHandler;

class UserDomainCall implements UserRepository
{
    private FetchUserDetailsByAccessCodeQueryHandler $fetchUserDetailsByAccessCodeQueryHandler;

    public function __construct(FetchUserDetailsByAccessCodeQueryHandler $fetchUserDetailsByAccessCodeQueryHandler)
    {
        $this->fetchUserDetailsByAccessCodeQueryHandler = $fetchUserDetailsByAccessCodeQueryHandler;
    }

    public function fetchUserIdByAccessToken(string $accessToken): ?int
    {
        $userData = $this->fetchUserDetailsByAccessCodeQueryHandler->handle($accessToken);

        return isset($userData['id']) ? (int) $userData['id'] : null;
    }
}
