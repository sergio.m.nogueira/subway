<?php declare(strict_types=1);

namespace App\Meal\Command\Infra\Repository\MealRepository;

use App\Meal\Command\Domain\Exception\AccessCodeAlreadyInUseException;
use App\Meal\Command\Domain\Exception\AnErrorOccurredWhileTryingToCloseMealException;
use App\Meal\Command\Domain\Exception\UnableToOpenMealRegistrationException;
use App\Meal\Command\Domain\Exception\UnableToSaveNewSandwichException;
use App\Meal\Command\Domain\Exception\UserAlreadyHasSandwichInTheMealException;
use App\Meal\Command\Domain\ValueObject\AccessCodeVO;
use DateTime;
use Illuminate\Support\Facades\DB;
use App\Meal\Command\Domain\Entity\MealEntity;
use App\Meal\Command\Domain\Repository\MealRepository;
use ReflectionClass;

class MySql implements MealRepository
{
    private const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    public function fetchOpenMeal(): ?MealEntity
    {
        $result = DB::selectOne('select id, access_code, created_at, status from meals where status = 1');

        return $result !== null ? $this->buildEntity($result) : null;
    }

    /**
     * @throws AccessCodeAlreadyInUseException
     * @throws UnableToOpenMealRegistrationException
     */
    public function saveNewMeal(MealEntity $meal): void
    {
        try {
            DB::insert(
                'insert into meals (access_code, status, created_at) values (?, ?, ?)',
                [
                    $meal->accessCode()->value(),
                    1,
                    $meal->openDate()->format(self::DATE_TIME_FORMAT)
                ]
            );
        } catch (\Throwable $exception) {
            if ((int) $exception->getCode() === 23000) {
                throw new AccessCodeAlreadyInUseException();
            } else {
                // TODO: Add log here
                throw new UnableToOpenMealRegistrationException();
            }
        }
    }

    /**
     * @throws UnableToSaveNewSandwichException
     * @throws UserAlreadyHasSandwichInTheMealException
     */
    public function saveMealSandwiches(MealEntity $meal): void
    {
        foreach ($meal->newSandwiches() as $newSandwich) {
            $characteristics = [
                'size' => $newSandwich->size(),
                'ovenBaked' => (int) $newSandwich->isOvenBaked(),
            ];

            $ingredients = [
                'bread' => $newSandwich->breadType(),
                'taste' => $newSandwich->taste(),
                'extras' => $newSandwich->extras(),
                'vegetables' => $newSandwich->vegetables(),
                'sauce' => $newSandwich->sauce(),
            ];

            try {
                DB::insert(
                    'insert into meal_sandwich (meal_id, user_id, characteristics, ingredients, created_at) values (?, ?, ?, ?, ?)',
                    [
                        $meal->id(),
                        $newSandwich->userId(),
                        json_encode($characteristics),
                        json_encode($ingredients),
                        $newSandwich->createdAt()->format(self::DATE_TIME_FORMAT),
                    ]
                );
            } catch (\Throwable $exception) {
                // TODO: Add log here
                if ((int) $exception->getCode() === 23000) {
                    throw new UserAlreadyHasSandwichInTheMealException(
                        json_encode(
                            [
                                'meal_id' => $meal->id(),
                                'user_id' => $newSandwich->userId(),
                            ]
                        )
                    );
                } else {
                    throw new UnableToSaveNewSandwichException();
                }
            }
        }
    }

    /**
     * @throws AnErrorOccurredWhileTryingToCloseMealException
     */
    public function saveClosedMeal(MealEntity $meal): void
    {
        try {
            DB::update(
                'UPDATE meals SET status = 0, closed_at = ? WHERE access_code = ? LIMIT 1',
                [
                    $meal->closeDate()->format(self::DATE_TIME_FORMAT),
                    $meal->accessCode()->value()
                ]
            );
        } catch (\Throwable $exception) {
            // TODO: Add log here
            throw new AnErrorOccurredWhileTryingToCloseMealException();
        }
    }

    /**
     * Probably this can be done with eloquent
     */
    private function buildEntity(\stdClass $result): MealEntity
    {
        $reflector = new ReflectionClass(MealEntity::class);

        /** @var MealEntity $entity */
        $entity = $reflector->newInstanceWithoutConstructor();

        $id = $reflector->getProperty('id');
        $id->setAccessible(true);
        $id->setValue($entity, $result->id);
        $id->setAccessible(false);

        $accessCode = $reflector->getProperty('accessCode');
        $accessCode->setAccessible(true);
        $accessCode->setValue($entity, new AccessCodeVO($result->access_code));
        $accessCode->setAccessible(false);

        $openedAt = $reflector->getProperty('openedAt');
        $openedAt->setAccessible(true);
        $openedAt->setValue($entity, DateTime::createFromFormat(self::DATE_TIME_FORMAT, $result->created_at));
        $openedAt->setAccessible(false);

        $status = $reflector->getProperty('status');
        $status->setAccessible(true);
        $status->setValue($entity, (int) $result->status);
        $status->setAccessible(false);

        return $entity;
    }
}
