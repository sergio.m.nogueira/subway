<?php declare(strict_types=1);

namespace App\Meal\Command\Infra\Service;

use App\Meal\Command\Domain\Service\AccessCodeGeneratorService;
use App\Meal\Command\Domain\ValueObject\AccessCodeVO;
use App\Shared\Service\PHPAccessCodeGeneratorService as SharedPHPAccessCodeGeneratorService;

class PHPAccessCodeGeneratorService
    extends SharedPHPAccessCodeGeneratorService
    implements AccessCodeGeneratorService
{
    public function newAccessCode(): AccessCodeVO
    {
        return new AccessCodeVO($this->generate());
    }
}
