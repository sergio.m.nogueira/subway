<p>A new meal was opened with the access code: <b>{{ $accessCode }}</b></p>
<p>To access meal details please <a href="/meal/details/{{ $accessCode }}">click here</a></p>
