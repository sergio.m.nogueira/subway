<html>

<head></head>

<body>

<h3>Open meal</h3>
@if (empty($openMeal))
    <p>No meal open</p>
    <a href="/meal?action=open">Open registration for a new meal</a>

    @isset($error)
        <p>An error occurred: {{ $error }}</p>
    @endisset
@else
    <p><a href="/meal/details/{{ $openMeal['accessCode'] }}">{{ $openMeal['accessCode'] }}</a> | {{ $openMeal['createdAt'] }} | <a href="/meal/?action=close">Close</a></p>
@endif

<h3>Closed meals</h3>
@if (empty($closedMeals))
    No closed meals
@else
    @foreach ($closedMeals as $closedMeal)
        <p><a href="/meal/details/{{ $closedMeal['accessCode'] }}">{{ $closedMeal['accessCode'] }}</a> | Open: {{ $closedMeal['createdAt'] }} | Closed: {{ $closedMeal['closedAt'] }}</p>
    @endforeach
@endif

</body>
</html>
