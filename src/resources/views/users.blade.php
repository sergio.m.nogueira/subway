<h4>Add new user</h4>
<form action="/user" method="post">
    @csrf
    <label for="name">Name</label>
    <input type="text" id="name" name="name">
    <input type="submit" value="Create user">
</form>

@if (!empty($saveError))
    {{ $saveError  }}
@endif

<h4>Existing users</h4>
@if(empty($users))
    <p>No users available</p>
@else
    @foreach ($users as $user)
        <p>{{ $user['accessCode'] }} | {{ $user['name'] }} | <a href="/meal/sandwich/{{ $user['accessCode'] }}" target="_blank">Order sandwich for {{ $user['name'] }}</a></p>
    @endforeach
@endif
