<h4>Details for meal {{ $mealAccessCode }}</h4>

@if(empty($mealDetails))
    No sandwiches were ordered
@else
    @foreach($mealDetails as $mealDetail)
        <p><b>User:</b> {{ $mealDetail['user'] }} | <b>Size:</b> {{ $mealDetail['size'] }} | <b>Oven baked:</b> {{ $mealDetail['ovenBaked'] }} | <b>Sauce:</b> {{ $mealDetail['sauce'] }} | <b>Extras:</b> {{ $mealDetail['extras'] }} | <b>Vegetables:</b> {{ $mealDetail['vegetables'] }}</p>
    @endforeach
@endif

<p><a href="/meal">Back to meals management</a></p>
