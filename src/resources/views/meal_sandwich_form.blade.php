<h4>Welcome USERNAME</h4>
<form action="/meal/sandwich/{{ $userAccessCode }}" method="post">
    @csrf
    <p>
        <label for="type">Bread type</label>
        <select id="type" name="type">
            @foreach ($types as $type)
                <option value="{{ $type['id'] }}">{{ $type['name'] }}</option>
            @endforeach
        </select>
    </p>

    <p>
        <label for="size">Sandwich size</label>
        <select id="size" name="size">
            <option value="15">15</option>
            <option value="30">30</option>
        </select>
    </p>

    <p>
        <input type="checkbox" name="oven-baked" id="oven-baked" />
        <label for="oven-baked">Oven baked</label>
    </p>

    <p>
        <label for="taste">Taste</label>
        <select id="taste" name="taste">
            @foreach ($tastes as $taste)
                <option value="{{ $taste['id'] }}">{{ $taste['name'] }}</option>
            @endforeach
        </select>
    </p>

    <fieldset>
        <legend>Extras</legend>
        @foreach ($extras as $extra)
            <p>
                <input type="checkbox" name="extras[]" id="{{ $extra['id'] }}" value="{{ $extra['id'] }}">
                <label for="{{ $extra['id'] }}">{{ $extra['name'] }}</label>
            </p>
        @endforeach
    </fieldset>

    <fieldset>
        <legend>Vegetables</legend>
        @foreach ($vegetables as $vegetable)
            <p>
                <input type="checkbox" name="vegetables[]" id="{{ $vegetable['id'] }}" value="{{ $vegetable['id'] }}">
                <label for="{{ $vegetable['id'] }}">{{ $vegetable['name'] }}</label>
            </p>
        @endforeach
    </fieldset>

    <p>
        <label for="sauce">Sauce</label>
        <select id="sauce" name="sauce">
            <option value=""></option>
            @foreach ($sauces as $sauce)
                <option value="{{ $sauce['id'] }}">{{ $sauce['name'] }}</option>
            @endforeach
        </select>
    </p>

    <input type="submit" value="Order sandwich"></p>
</form>
