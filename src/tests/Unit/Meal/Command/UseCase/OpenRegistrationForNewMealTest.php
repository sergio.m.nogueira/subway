<?php declare(strict_types=1);

namespace Tests\Unit\Meal\Command\UseCase;

use App\Meal\Command\Domain\Entity\MealEntity;
use App\Meal\Command\Domain\Exception\AMealIsAlreadyOpenException;
use App\Meal\Command\Domain\Repository\MealRepository;
use App\Meal\Command\Domain\Service\AccessCodeGeneratorService;
use App\Meal\Command\UseCase\OpenRegistrationForNewMeal\Handler;
use PHPUnit\Framework\TestCase;

class OpenRegistrationForNewMealTest extends TestCase
{
    private MealRepository $mealRepository;
    private AccessCodeGeneratorService $accessCodeGeneratorService;

    protected function setUp(): void
    {
        $this->mealRepository = $this->createMock(MealRepository::class);
        $this->accessCodeGeneratorService = $this->createMock(AccessCodeGeneratorService::class);
    }

    public function testIfTryToOpenRegistrationForANewMealWhileAnotherMealIsStillOpenShouldThrowException(): void
    {
        $this->expectException(AMealIsAlreadyOpenException::class);

        $this->mealRepository->method('fetchOpenMeal')
            ->willReturn($this->createMock(MealEntity::class));

        $this->openRegistration();
    }

    public function testIfTryToOpenRegistrationForANewMealAndNoMealIsOpenAlreadyShouldCreateItAndStoreInDb(): void
    {
        $this->mealRepository->method('fetchOpenMeal')
            ->willReturn(null);

        $this->mealRepository->expects($this->once())
            ->method('saveNewMeal')
            ->with($this->isInstanceOf(MealEntity::class));

        $this->openRegistration();
    }

    private function openRegistration(): void
    {
        $useCase = new Handler($this->mealRepository, $this->accessCodeGeneratorService);
        $useCase->open();
    }
}
