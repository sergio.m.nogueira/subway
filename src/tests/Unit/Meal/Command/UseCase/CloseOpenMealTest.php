<?php declare(strict_types=1);

namespace Tests\Unit\Meal\Command\UseCase;

use App\Meal\Command\Domain\Entity\MealEntity;
use App\Meal\Command\Domain\Exception\NoOpenMealException;
use App\Meal\Command\Domain\Repository\MealRepository;
use App\Meal\Command\UseCase\CloseOpenMeal\Handler;
use PHPUnit\Framework\TestCase;

class CloseOpenMealTest extends TestCase
{
    private MealRepository $mealRepository;

    protected function setUp(): void
    {
        $this->mealRepository = $this->createMock(MealRepository::class);
    }

    public function testIfThereIsNoOpenMealShouldThrowException(): void
    {
        $this->expectException(NoOpenMealException::class);

        $this->mealRepository->method('fetchOpenMeal')
            ->willReturn(null);

        $this->closeOpenMeal();
    }

    public function testIfThereIsAnOpenMealShouldCloseItAndSaveIt(): void
    {
        $meal = $this->createMock(MealEntity::class);
        $meal->expects($this->once())
            ->method('close');

        $this->mealRepository->method('fetchOpenMeal')
            ->willReturn($meal);

        $this->mealRepository->expects($this->once())
            ->method('saveClosedMeal')
            ->with($meal);

        $this->closeOpenMeal();
    }

    private function closeOpenMeal(): void
    {
        (new Handler($this->mealRepository))->handle();
    }
}
