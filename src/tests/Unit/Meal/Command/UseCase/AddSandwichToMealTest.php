<?php declare(strict_types=1);

namespace Tests\Unit\Meal\Command\UseCase;

use App\Meal\Command\Domain\Entity\MealEntity;
use App\Meal\Command\Domain\Exception\InvalidAccessCodeException;
use App\Meal\Command\Domain\Exception\MealIsClosedException;
use App\Meal\Command\Domain\Repository\MealRepository;
use App\Meal\Command\Domain\Repository\UserRepository;
use App\Meal\Command\UseCase\AddSandwichToMeal\Command;
use App\Meal\Command\UseCase\AddSandwichToMeal\Handler;
use PHPUnit\Framework\TestCase;

class AddSandwichToMealTest extends TestCase
{
    private const USER_ID = 100;
    private const BREAD_TYPE = 1;
    private const SIZE = 15;
    private const TASTE = 2;
    private const SAUCE = 3;
    private const EXTRAS = [4];
    private const VEGETABLES = [5, 6];

    private MealRepository $mealRepository;
    private UserRepository $userRepository;
    private MealEntity $meal;

    protected function setUp(): void
    {
        $this->mealRepository = $this->createMock(MealRepository::class);
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->meal = $this->createMock(MealEntity::class);
    }

    public function testIfThereIsNoOpenMealShouldThrowException(): void
    {
        $this->expectException(MealIsClosedException::class);

        $this->mealRepository->method('fetchOpenMeal')
            ->willReturn(null);

        $this->addSandwichToMeal();
    }

    public function testIfThereIsNoUserShouldThrowException(): void
    {
        $this->expectException(InvalidAccessCodeException::class);

        $this->mealRepository->method('fetchOpenMeal')
            ->willReturn($this->meal);

        $this->userRepository->method('fetchUserIdByAccessToken')
            ->willReturn(null);

        $this->addSandwichToMeal();
    }

    public function testIfEverythingIsOkShouldAddSandwichToMealAndSaveIt(): void
    {
        $this->meal->expects($this->once())
            ->method('addNewSandwich')
            ->with(
                self::USER_ID,
                self::BREAD_TYPE,
                self::SIZE,
                false,
                self::TASTE,
                self::EXTRAS,
                self::VEGETABLES,
                self::SAUCE
            );

        $this->mealRepository->method('fetchOpenMeal')
            ->willReturn($this->meal);

        $this->userRepository->method('fetchUserIdByAccessToken')
            ->willReturn(self::USER_ID);

        $this->addSandwichToMeal();
    }

    private function addSandwichToMeal(): void
    {
        $command = new Command('', self::BREAD_TYPE, self::SIZE, false, self::TASTE, self::EXTRAS, self::VEGETABLES, self::SAUCE);
        (new Handler($this->mealRepository, $this->userRepository))->handle($command);
    }
}
