<?php

use App\Meal\Controller\MealDetailsController;
use App\Meal\Controller\SandwichController;
use App\Meal\Controller\MealController;
use App\User\Controller\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::match(['get'], '/meal', MealController::class);
Route::match(['get'], '/meal/details/{mealAccessCode}', MealDetailsController::class);
Route::match(['get', 'post'], '/meal/sandwich/{userAccessCode}', SandwichController::class);
Route::match(['get', 'post'], '/user', UserController::class);
