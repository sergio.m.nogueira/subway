<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealSandwichTable extends Migration
{
    private const TABLE_NAME = 'meal_sandwich';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('meal_id');
            $table->unsignedBigInteger('user_id');
            $table->json('characteristics');
            $table->json('ingredients');
            $table->timestamps();
            $table->foreign('meal_id')->references('id')->on('meals');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unique(['meal_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
