<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSandwichIngredientsTable extends Migration
{
    private const TABLE_NAME = 'sandwich_ingredients';

    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name', 30);
            $table->enum('type', ['bread_type', 'taste', 'extra', 'vegetable', 'sauce']);
            $table->index('type');
        });
    }

    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
