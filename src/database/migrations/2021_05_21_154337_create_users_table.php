<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 30);
            $table->string('access_code', 30);
            $table->timestamp('created_at', 0);
            $table->unique('access_code');
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
