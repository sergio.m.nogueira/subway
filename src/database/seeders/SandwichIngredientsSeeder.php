<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class SandwichIngredientsSeeder extends Seeder
{
    private const COMPONENTS = [
        // Bread types
        [
            'name' => 'Malted Rye',
            'type' => 'bread_type',
        ],
        [
            'name' => 'Italian Herb & Cheese',
            'type' => 'bread_type',
        ],
        [
            'name' => 'White Bread',
            'type' => 'bread_type',
        ],
        // Tastes
        [
            'name' => 'Chicken Fajita',
            'type' => 'taste',
        ],
        [
            'name' => 'Oven Breaded Chicken',
            'type' => 'taste',
        ],
        [
            'name' => 'Smoked Turkey',
            'type' => 'taste',
        ],
        // Extras
        [
            'name' => 'Extra bacon',
            'type' => 'extra',
        ],
        [
            'name' => 'Double meat',
            'type' => 'extra',
        ],
        [
            'name' => 'Extra Cheese',
            'type' => 'extra',
        ],
        // Vegetables
        [
            'name' => 'Avocado',
            'type' => 'vegetable',
        ],
        [
            'name' => 'Capsicum',
            'type' => 'vegetable',
        ],
        [
            'name' => 'Carrots',
            'type' => 'vegetable',
        ],
        // Sauces
        [
            'name' => 'Habanero Hot Sauce',
            'type' => 'sauce',
        ],
        [
            'name' => 'Blue Cheese Dressing',
            'type' => 'sauce',
        ],
        [
            'name' => 'Tomato Sauce',
            'type' => 'sauce',
        ],
    ];

    public function run()
    {
        foreach (self::COMPONENTS as $component) {
            DB::table('sandwich_ingredients')->insert(
                [
                    'name' => $component['name'],
                    'type' => $component['type']
                ]
            );
        }
    }
}
